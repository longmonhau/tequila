<?php namespace lOngmon\Hau\Kernel\Component;

use lOngmon\Hau\Kernel\Factory;

class Captcha {
    
    private $captcha = null;

    private static $_this = null;
    
    public function __construct(){
        $this->captcha = Factory::make( 'Captcha' );
    }
    
    public function build($width = 150, $height = 40, $font = null) {
        $this->captcha->build( $width, $height, $font );
        $_SESSION['Captcha_Code'] = $this->captcha->getPhrase();
        return $this;
    }

    public function getPhrase() {
        return $this->captcha->getPhrase();
    }
    public function testPhrase( $phrase ) {
        return $this->captcha->testPhrase($phrase);
    }
    
    public function Base64enCode() {
        return $this->captcha->inline();
    }
    public static function newInstance( $param = [] ) {
        if ( self::$_this == null ) {
             self::$_this = new self();
        }
        return self::$_this;
    }
}