<?php namespace lOngmon\Hau\Kernel\Component;

class Validate {
    public static function is_AlphaNumeric( $str ) {
        return preg_match('/^[\w]+$/i', $str );
    }
    
    public static function is_Word( $str ) {
        return preg_match('/^[a-zA-Z]+$/i', $str );
    }
    
    public static function is_Digit( $int ) {
        return is_numeric( $int );
    }
    
    public static function is_UnicodeWord() {
        return preg_match('/^[\x{4e00}-\x{9fa5}\w]+$/ui', $str );
    }
    
    public static function get_AlphaNumeric( $str ) {
        return preg_replace('/^[^\w]+$/','', $str );
    }
}