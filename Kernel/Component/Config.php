<?php namespace lOngmon\Hau\Kernel\Component;

class Config {
    
    private static $cfg = [];
    
    private static function loadCfg() {
        if ( !empty( $cfg ) ) {
            return true;
        }
        $cfg_file = APP_PATH.'/Config/common.php';
        if ( !is_file($cfg_file) ) {
            throw new \exception("Configure file dose not exist");
        }
        self::$cfg = include( $cfg_file );
    }
    
    public static function get( $key, $default = null ) {
        if ( empty( self::$cfg ) ) {
            self::loadcfg();
        }
        $temp = self::$cfg;
        $key_array = explode('.', $key );
        $key_len = count($key_array);
        for ( $i=0; $i<$key_len; $i++) {
            if (!$k = array_shift($key_array) ){
                return $temp;
            }
            if (!isset($temp[$k])) {
                return $default;
            } else {
                $temp = $temp[$k];
            }
        }
        return $temp;
    }
    public static function set( $key, $val ) {
        if ( empty( self::$cfg ) ) {
            self::loadcfg();
        }
        self::$cfg[$key] = $val;
        return $val;
    }
}