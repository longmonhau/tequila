<?php namespace lOngmon\Hau\Kernel\Component;
/**
 * AES 加密类
 * @package Component
 * @author lOngmon Hau <longmon.hau@gmail.com>
 */
class AES{
	private static $key = '';

	private static $iv = '';

	private static $cipher = null;
	
	/**
	 * AES 加密静态方法
	 * @param $text string 待加密明文
	 * @param $key string 密匙
	 * @return string 返回16进制的密文
	 */
	public static function Encrypt( $text, $key ) {
		self::$cipher = mcrypt_module_open( MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '' );
		$text = self::pad( $text );
		if ( mcrypt_generic_init( self::$cipher, self::key( $key ), self::iv() ) == -1 ){
			return false;
		}
		$encryptedText = mcrypt_generic( self::$cipher, $text );
		self::descMcrypt();
		return bin2hex( self::$iv.$encryptedText );
	}
	
	/**
	 * AES 静态解密方法
	 * @param $ecnrypted string 16进制的密文
	 * @param $key string 密匙
	 * @return string 返回解密明文
	 */
	public static function Decrypt( $ecnrypted, $key ){
		self::$cipher = mcrypt_module_open( MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '' );
		$enc = hex2bin( $ecnrypted );
		$iv_size = mcrypt_enc_get_iv_size( self::$cipher );
		$iv = substr( $enc, 0, $iv_size );
		$encBin = substr( $enc, $iv_size );
		if ( mcrypt_generic_init( self::$cipher, self::key( $key ), $iv ) == -1 ){
			return false;
		}
		$decryptText = mdecrypt_generic( self::$cipher, $encBin );
		self::descMcrypt();
		return self::unpad( $decryptText );
	}
	/**
	 * 生成密匙
	 */
	private static function key( $key ){
		return self::$key = hash( 'MD5', $key, true );
	}
	/**
	 * 生成向量
	 */
	private static function iv(){
		$iv_size = mcrypt_enc_get_iv_size( self::$cipher );
		self::$iv = mcrypt_create_iv( $iv_size );
		return self::$iv;
	}
	/**
	 * 明文补长
	 */
	private static function pad( $text ){
		$block_size = mcrypt_get_block_size( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC );
		$pad_size = $block_size - strlen( $text )%$block_size;
		$text .= str_repeat( chr($pad_size), $pad_size );
		return $text;
	}
	/**
	 * 明文反补长
	 */
	private static function unpad( $str ){
		$strlen = strlen( $str );
		$pad_size = ord( $str[$strlen-1] );
		return substr($str, 0, $strlen - $pad_size );
	}
	/**
	 * 析构加密模块
	 */
	private static function descMcrypt(){
		mcrypt_generic_deinit( self::$cipher );
		mcrypt_module_close( self::$cipher );
	}
}