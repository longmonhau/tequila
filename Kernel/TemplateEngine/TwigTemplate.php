<?php namespace lOngmon\Hau\Kernel\TemplateEngine;

use lOngmon\Hau\Kernel\Component\Config;

class TwigTemplate implements TemplateInterFace {
    
    private $twig = null;
    
    private $_vars = [];
    
    public function __construct() {
        $template_dir = Config::get('TEMPLATE_DIR');
        $loader = new \Twig_Loader_Filesystem([$template_dir.'/'.Config::get('TEMPLATE_THEME'), $template_dir.'/Common']);
        $option = [];
        if ( Config::get("TEMPLATE_CACHE") ) {
            $option['cache'] = Config::get( 'TEMPLATE_CACHE_DIR' );
        }
        $option['debug'] = Config::get('TEMPLATE_DEBUG');
        $option['charset'] = Config::get('TEMPLATE_CHARSET');
        $option['autoescape'] = Config::get("TEMPLATE_AUTOESCAPE");
        $option['optimizations'] = Config::get('TEMPLATE_OPTIMIZATION');
        $this->twig = new \Twig_Environment( $loader, $option );
    }
    
    public function render( $file, $args = [] ) {
        if ( !empty($args) ) {
            $this->assign($args);
        }
        return $this->twig->render( $file, $this->_vars );
    }
    
    public function assign( $key, $val=[] ) {
        if ( is_array( $key ) ){
            $this->_vars = $key;
            return;
        }
        $this->_vars[$key] = $val;
    }
}