<?php namespace lOngmon\Hau\Kernel\TemplateEngine;

interface TemplateInterFace {
    public function render( $file, $args = [] );
    public function assign( $key, $val );
}