<?php namespace lOngmon\Hau\Kernel\Service;

class tRedis {
        private static $Redis = null;
        public static function newInstance(){
                if ( self::$Redis != null ) {
                        return self::$Redis;
                }
                if ( !class_exists( "\Redis" ) ) {
                        return false;
                }
                $redisCfg = include(APP_PATH.'/Config/Redis.php');
                self::$Redis = new \Redis;
                $conn = self::$Redis->connect( $redisCfg['host'], $redisCfg['port'] );
                if ( !$conn ) {
                        throw new \exception("Redis Server Connect Failed");
                }
                return self::$Redis;
        }
        
        public static function __callStatic( $name, $param = [] ) {
                self::newInstance();
                if ( !(self::$Redis instanceof \Redis) ) {
                        return false;
                }
                if ( method_exists( __CLASS__, $name ) ) {
                        return call_user_func_array( [__CLASS__, $name ], $param );
                }
                if ( method_exists(self::$Redis, $name) ) {
                        return call_user_func_array( [self::$Redis, $name], $param );
                }
        }
}