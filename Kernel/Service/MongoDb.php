<?php namespace lOngmon\Hau\Kernel\Service;

use lOngmon\Hau\Kernel\Exception\ConfigException;

class MongoDb{

    private static $MDC = [];
    private static $MongoDB = [];
    private static $host = [];
    
    private function __construct(){}
    
    public function __clone(){}
    
    /**************************************************
     * 连接实例
     * @param $name string
     * @return MongoDBClient
     *
     *************************************************/
    public static function Connect($name){
        if(isset(self::$MDC[$name])){
            return self::$MDC[$name];
        }
        if(isset(self::$MDC['default'])){
            return self::$MDC['default'];
        }else{
            return null;
        }
        if(!$host = self::loadConfigFile()){
            return null;
        }
        if(!isset($host[$name])){
            if( isset($host['default']) ){
                $name = 'default';
            }else{
                return null;
            }
        }
        $dsn = "mongodb://".$host[$name]['host'].":".$host[$name]['port'];
        self::$MDC[$name] = new \MongoClient($dsn);
        return self::$MDC[$name];
    }

    /***************************************************
     * 数据库实例
     * @param $name string
     * @retrun MongoDB
     **************************************************/
    public static function DBInstance($name){
        if(!self::loadConfigFile()){
            return null;
        }
        if(isset(self::$MongoDB[$name])){
            return self::$MongoDB[$name];
        }
        if(isset(self::$MDC[$name])){
            self::$MDC[$name]->selectDB(self::$host[$name]['dbname']);
        }
        if(!$mdc = self::Connect($name)){
            return null;
        }
        self::$MongoDB[$name] = $mdc->selectDB(self::$host[$name]['dbname']);
        return self::$MongoDB[$name];
    }

    /***************************************************
     * 载入配置文件
     **************************************************/
    private static function loadConfigFile(){
        if(!empty(self::$host)){
            return self::$host;
        }
        if(!is_file( APP_PATH.'/Config/Mongo.php')){
            return null;
        }
        $config = include(APP_PATH.'/Config/Mongo.php');
        if(!is_array($config)){
            return null;
        }

        foreach( $config as $name => $c ){
            if( !is_array($c) ) {
                continue;
            }
            if( !isset($c['host']) ){
                continue;
            }
            self::$host[$name]['host'] = $c['host'];
            if( !isest( $c['port']) ) {
                $c['port'] = 27017;
            }
            self::$host[$name]['port'] = $c['port'];
            if( !isset($c['dbname'])){
                $c['dbname'] = 'tequila';
            }
            self::$host[$name]['dbname'] = $c['dbname'];
        }
        return self::$host;
    }
}