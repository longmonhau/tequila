<?php namespace lOngmon\Hau\Kernel;

use lOngmon\Hau\Kernel\Factory;

class Provider {
    
    private static $dependency = [];
    
    private static $midware = [];
    
    public static function systemDepes() {
        self::$dependency = [
            "Response" => "lOngmon\Hau\Kernel\Http\Response",
            'Request'  => "lOngmon\Hau\Kernel\Http\Request",
            'Session' => 'lOngmon\Hau\Kernel\Http\Session',
            'Twig'     => "lOngmon\Hau\Kernel\TemplateEngine\TwigTemplate",
            'Captcha'  => "lOngmon\Hau\Kernel\Http\Captcha"
        ];    
    }
    
    public static function registry() {
        self::systemDepes();
        foreach (self::$dependency as $key => $deps) {
            Factory::Registry( $key, $deps );
        }
    }

    public static function midwareRegistry(){
           self::$midware = [
                    'Auth' => 'lOngmon\Hau\Usr\MidWare\AdminAuthorize'
           ];
           foreach ( self::$midware as $name => $mw ) {
               Factory::midwareRegistry( $name, $mw );
           }
           return;
    }
}