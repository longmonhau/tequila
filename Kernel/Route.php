<?php namespace lOngmon\Hau\Kernel;

class Route {
    
    private $method;
    
    private static $instance = null;
    
    private $fastRoute = null;
    
    private static $route_array = [];
    
    public static function newInstance( $method ) {
        return self::$instance = new self($method);
    }
    
    private function __construct( $method ) {
        $this->method = $method;
        $route_file = APP_PATH.'/Config/Routes.php';
        if ( !is_file( $route_file ) ) {
            throw new \exception( "路由文件未定义", 500 );
        }
        $this->fastRoute = \FastRoute\SimpleDispatcher(function( \FastRoute\RouteCollector $R) {
            $routes = include( APP_PATH.'/Config/Routes.php' );
            foreach ( $routes as $name =>$rt ) {
                if ( is_string( $name ) ) {
                      self::routeRegistry( $name, $rt );
                }
                $R->addRoute( $rt[0], $rt[1], $rt[2] );
            }
        });
    }
    
    public function dispatch( $uri ) {
        $routeInfo = $this->fastRoute->dispatch( $this->method, $uri );
        //var_dump( $routeInfo ); exit;
        switch( $routeInfo[0] ){
            case \FastRoute\Dispatcher::NOT_FOUND:
                return ["Controller"=>"Common\Error","Action"=>"_40x", "params"=>['code'=>404]];
                break;
            case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                return ["Controller"=>"Common\Error","Action"=>"_40x","params"=>['code'=>403]];
                break;
            case \FastRoute\Dispatcher::FOUND:
                return $this->_dispatch($routeInfo[1], $routeInfo[2]);
                break;
        }
    }
    
    private function _dispatch( $routeinfo, $params = [] ) {
        $dispatch_result = [];
        if ( is_callable( $routeinfo ) ) {
            $dispatch_result['callable'] = call_user_func_array( $routeinfo, $params );
            return $dispatch_result;
        }
        
        if ( is_array( $routeinfo ) ) {
            if ( count( $routeinfo ) < 2 ){
                return $this->_dispatch( array_shift( $routeinfo ), $params );
            } else {
                if ( count($routeinfo) ==3 ) {
                    list($dispatch_result['Controller'], $dispatch_result['Action'],$dispatch_result['midware']) = $routeinfo;
                } else {
                     list($dispatch_result['Controller'], $dispatch_result['Action']) = $routeinfo;
                }
               
                if ( !empty($params) ) {
                    $dispatch_result['params'] = $params;
                }
                return $dispatch_result;
            }
        }
        if ( class_exists( $routeinfo ) ) {
            $dispatch_result['class'] = $routeinfo;
            if ( !empty($params) ) {
                $dispatch_result['params'] = $params;
            }
            return $dispatch_result;
        }
        return (string)$routeinfo;
    }

                private static function routeRegistry( $name, $route ) {
                        self::$route_array[$name] = $route;
                }

                public static function getRoute( $name ) {
                               if ( isset( self::$route_array[$name]) ) {
                                       return $route_array[$name];
                               }
                               return null;
                }

                public static function redirect( $name ) {
                              if ( isset( self::$route_array[$name] ) ) {
                                            $route = self::$route_array[$name];
                                            header('location: '.$route[1] );
                              } else {
                                            header('location: '. $name );
                              }
                }
}