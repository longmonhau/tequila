<?php namespace lOngmon\Hau\Kernel;

use lOngmon\Hau\Kernel\Route;
use lOngmon\Hau\Kernel\SimpleDi;
use lOngmon\Hau\Kernel\tSession;
use lOngmon\Hau\Kernel\Response;

class BootStrap {

    private static $route = null;

    public static function init() {
        if ( version_compare( PHP_VERSION, 5.5, "<" ) ) {
            exit( "PHP required 5.5+" );
        }
        register_shutdown_function( array( __CLASS__, 'shutDown' ) );
        set_exception_handler( array(__CLASS__, 'exceptionHandler') );
        
        Provider::registry();
        Provider::midwareRegistry();
        tSession::start();
    }
    
    public static function route() {
        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $slug = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );
        $slug = str_replace( ['index.php','.html','.htm'], '', $slug );
        $route = Route::newInstance( $httpMethod );
        return $route->dispatch( strtolower($slug) );
    }
    
    public static function run() {
        self::init();
        self::$route = self::route();
        if ( isset(self::$route['callable']) ) {
            $resp = new Response( self::$route['callable'] );
            return $resp->toString();
        }
        if ( isset(self::$route['class']) ) {
            $params = isset( self::$route['params'] )?self::$route['params']:array();
            $resp = new Response( Factory::make( self::$route['class'], $params ) );
            return $resp->toString();
        }
        
        $ctrl = null;
        if ( isset( self::$route['Controller'] ) ) {
            $ctrl = new SimpleDi( self::$route['Controller'] );
        }
        if ( $ctrl && isset( self::$route['Action'] ) ) {
            /**调用中间件*/
            self::midware();
            $params = isset( self::$route['params'] )?self::$route['params']:[];
            $ctrl->invoke( self::$route['Action'], $params );
            return;
        }
        throw new \Exception("Route Failed", 500);
    }
    
    public static function shutDown() {
        //var_dump("shut down ...");
    }
    public static function exceptionHandler($e) {
        echo "<h2>".$e->getMessage()."</h2> in file ".$e->getFile()." on line ".$e->getLine();
    }

    private static function midware() {
              if ( !isset( self::$route['midware'] ) ) {
                   return true;
              }
              foreach ( self::$route['midware'] as $mw ) {
                    if ( $midware = Factory::midware( $mw ) )
                     {  
                            $midware->run(); 
                    }
              }
    }
}