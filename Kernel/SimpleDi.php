<?php namespace lOngmon\Hau\Kernel;

use lOngmon\Hau\Kernel\Factory;

class SimpleDi {

    private $classReflection = null;
    
    private $class = '';
    
    private $methodReflect = null;

    public function __construct( $class ) {
        
        $this->instance( $class );
    }
    
    private function instance( $class ) {
        $namespace = 'lOngmon\Hau\Usr\Controller\\';
        if( class_exists( $class ) ) {
            $this->class = $class;
            return $this->classReflection = new \ReflectionClass( $class );
        } elseif ( class_exists( $namespace.$class ) ) {
            $this->class = $namespace.$class;
            return $this->classReflection = new \ReflectionClass( $namespace.$class );
        } else {
            throw new \Exception( "Class '{$class}' dose not exist!", 500 );
        }
        return null;
    }
    
    private function getMethod( $method ) {
        if ( $this->classReflection->hasMethod( $method ) ) {
            return $this->methodReflect = $this->classReflection->getMethod( $method );
        } else {
            throw new \exception("Method {$this->class}::{$method} do not exist", 500 );
        }
        return null;
    }
    
    private function getParameters( $params = [] ) {
        $parameterRef = $this->methodReflect->getParameters();
        $param_array = [];
        foreach ( $parameterRef as $pmRef ) {
            $pmClass = $pmRef->getClass();
            $pclass = $pmClass?$pmClass->name:null;
            $param_array[$pmRef->name] = $pclass?Factory::make( $pclass ):null;
        }
        return array_merge( $param_array, $params );
    }
    
    public function invoke( $method, $args = [] ) {
        
        $this->getMethod( $method );
        
        $paramers = $this->getParameters( $args ); //var_dump( $paramers );
        
        $instance = $this->classReflection->newInstance();
        if ( !empty( $paramers ) ) {
            return $this->methodReflect->invokeArgs( $instance, $paramers );
        } else {
            return $this->methodReflect->invoke( $instance );
        }
    }
}