<?php namespace lOngmon\Hau\Kernel;

use  Illuminate\Database\Eloquent\Model  as Eloquent; 

class Model extends Eloquent {
	
	private static $_this = NULL;

	public static function __callStatic( $name, $args = [] ) {
		self::_newInstance();
		if ( method_exists( self::$_this, $name ) ) {
			return call_user_func_array([self::$_this, $name], $args );
		}
		return null;
	}

	public static function _newInstance() {
		if ( !self::$_this ) {
			self::$_this = new self();
		}
		return self::$_this;
	}
}