<?php namespace lOngmon\Hau\Kernel;

use lOngmon\Hau\Kernel\Factory;
use lOngmon\Hau\Kernel\Response;

class Control {
    
    private $tpl_vars = [];
    
    private $Response = null;

    protected $Request = null;
    
    public function __construct() {
            // $this->Request = Factory::make( 'Request' );
    }
    
    protected function render( $html ) {
        $this->Response = new Response($html, $this->tpl_vars);
        $this->Response->toHtml();
    }
    
    protected function assign( $key, $val ) {
        $this->tpl_vars[$key] = $val;
    }

    protected function renderJson( $out ) {
          $this->Response = new Response( (array)$out );
          $this->Response->toJson();
    }
    protected function renderString( $str ) {
          $this->Response = new Response( (string)$str );
          $this->Response->toString();
    }
}