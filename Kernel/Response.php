<?php namespace lOngmon\Hau\Kernel;

use lOngmon\Hau\Kernel\SimpleRender;
use lOngmon\Hau\Kernel\Factory;
use lOngmon\Hau\Kernel\Component\Config;

class Response {
    
    private $response = null;

    private $mix = null; 

    private $args = [];
    
    public function __construct( $mix, $args = [] ) {
        $this->mix = $mix;
        $this->args = $args;
        if ( is_callable( $mix ) ) {
            $this->mix = call_user_func_array( $mix, $args );
        }
        $this->response = Factory::make('Response');
    }
    
    /**
     * 输出字符串
     * @return [type] [description]
     */
    public function toString(){
        $this->response->headers->set('Content-Type', 'text/plain');
        if ( is_object( $this->mix ) ) {
                //对象必须输出内容
                $this->response->setContent( $this->mix );
        } else if( is_array( $this->mix )) {
                 $this->response->setContent( join(' ',$this->mix ) );
        } else {
                  $this->response->setContent( $this->mix );
        }
        $this->send();
    }
    
    public function toJson() {
        $this->response->headers->set('Content-Type', 'application/json');
        if ( is_array( $this->mix ) || is_object( $this->mix ) ) {
              $this->response->setContent( json_encode( $this->mix ) );
        } else if( is_string( $this->mix ) ) {
               $this->response->setContent( json_encode([$this->mix]) );
        }
        $this->send();
    }
    
    public function toHtml(){
        $this->response->headers->set('Content-Type', 'text/html');
        $render = new SimpleRender;
        $render->assign( $this->args );
        $this->response->setContent( $render->render( $this->mix ) );
        $this->send();
    }
    
    public function send(){
        $this->response->send();
    }
}