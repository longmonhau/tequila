<?php namespace lOngmon\Hau\Kernel;

use lOngmon\Hau\Kernel\Factory;

class tSession{
	public static function start() {
		$session = Factory::make( 'Session' );
		$session->start();
	}
}