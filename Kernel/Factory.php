<?php namespace lOngmon\Hau\Kernel;

class Factory {
    
    private static $deps = [];

    private static $midware = [];
    
    private static $instance = [];
    
    public static function registry( $name, $class ) {
        self::$deps[$name] = $class;
    }

    public static function midwareRegistry( $name, $midware ) {
          self::$midware[$name] = $midware;
    }
    
    public static function make( $className, $params = [] ) {
        if ( isset(self::$instance[(string)$className] )) {
            return self::$instance[$className];
        }
        $class = '';
        if ( isset(self::$deps[(string)$className] )) {
            $param_default = [];
            if ( is_array(self::$deps[$className]) ) {
                $class = array_shift( self::$deps[$className] );
                $param_default = (array)array_shift( self::$deps[$className] );
                $params = array_merge( $param_default, $params );
            } else {
                $class = self::$deps[$className];
            }
        } else if ( class_exists( $className ) ) {
            $class = $className;
        } else {
              throw new \Exception("Class {$className} dose not exist!", 1);
        }
        $classRef = new \ReflectionClass( $class );
        if ($classRef->hasMethod('newInstance') ) {
            self::$instance[$className] = call_user_func_array( [$class, 'newInstance'], $params );
            return self::$instance[$className];
        }
        if ( $classRef->hasMethod('__construct') ) {
            self::$instance[$className] = $classRef->newInstanceArgs($params);
            return self::$instance[$className];
        }
        self::$instance[$className] = $classRef->newInstance();
        return self::$instance[$className];
    }

    public static function midware( $name ) {
           if ( isset( self::$instance[$name] ) ) {
                    return self::$instance[$name];
           }
           if ( !isset( self::$midware[$name] ) ) {
                  return null;
           }

           if ( !class_exists( self::$midware[$name] ) ) {
                    throw new \Exception("Class ".self::$midware[$name]." dose not exist", 1);
                    return null;
           }

           $classRef = new \ReflectionClass( self::$midware[$name] );
           return $classRef->newInstance();
    }
}