<?php namespace lOngmon\Hau\Kernel;

class Collector {
    
    private static $ware = [];
    
    public static function get( $key, $default = null ) {
        if ( isset( self::$ware[$key] ) ) {
            return self::$ware[$key];
        }
        return $default;
    }
}