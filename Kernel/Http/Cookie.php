<?php namespace lOngmon\Hau\Kernel\Http;

use Symfony\Component\HttpFoundation\Cookie;

class tCookie extends Cookie{
    public static function newInstance($name, $value = null, $expire = 0, $path = '/', $domain = null, $secure = false, $httpOnly = true) {
        return new self( $name, $value, $expire, $path, $domain, $secure, $httpOnly );
    }
}