<?php namespace lOngmon\Hau\Kernel\Http;

use Symfony\Component\HttpFoundation\Session\Session as SymfonySession;

class Session extends SymfonySession {
	public static function  newInstance(){
		return new self();
	}
}