<?php namespace lOngmon\Hau\Kernel\Http;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class Response extends SymfonyResponse {
    public static function newInstance(){
        return new self();
    }
}