<?php namespace lOngmon\Hau\Kernel\Http;

use Gregwar\Captcha\CaptchaBuilder;

class Captcha extends CaptchaBuilder {
    public static function newInstance() {
        return new self();
    }
}