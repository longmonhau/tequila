<?php namespace lOngmon\Hau\Kernel\MidWare;

abstract class  MidWareAbstract {
	public function run(){
		if ( $this->assert() ) {
			$this->goNext();
		} else {
			$this->falseHandler();
		}
	}
	abstract protected function assert();
	abstract protected function goNext();
	abstract protected function falseHandler();
}