<?php namespace lOngmon\Hau\Kernel;

use lOngmon\Hau\Kernel\Component\Config;
use lOngmon\Hau\Kernel\Factory;

class SimpleRender {
    
    private $engine = null;
    
    public function __construct() {
        $engine = Config::get('TEMPLATE_ENGINE');
        $this->engine = Factory::make( $engine );
    }
    
    public function assign( $key, $val = [] ) {
        $this->engine->assign( $key, $val );
    }
    public function render($file) {
        return $this->engine->render( $file );
        
    }
}