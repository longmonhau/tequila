<?php
$pdo = new PDO('mysql:host=127.0.0.1;dbname=tequila;','root','');
$tables = [];
$tables['admin'] = <<<TABLE_CREATE
create table if not exists admin(
id int not null auto_increment,
name char(20) not null,
email varchar(128) not null,
rule tinyint not null default 0,
password char(60) not null,
created_at timestamp,
updated_at timestamp not null,
last_login_at timestamp,
last_login_ip timestamp,
primary key(id),
unique(name)
) engine=innodb,default charset=utf8;
TABLE_CREATE;

$tables['post'] = <<<TABLE_CREATE
create table if not exists post_meta(
id int not null auto_increment,
url char(50) not null,
title varchar(120) not null,
author int not null,
)
TABLE_CREATE;


foreach ( $tables as $tb => $sql ) {
    echo "Creating table {$tb} ";
    $pdo->query($sql);
    echo "................... done!\n";
}

$inserts =[];
$password = password_hash('123456',PASSWORD_DEFAULT);
$inserts['admin'] = <<<INSERTSQL
insert into admin(`name`,`email`,`password`,`created_at`,`updated_at`) values('hau','1307995200@qq.com',"{$password}",now(),now());
INSERTSQL;

foreach ( $inserts as $tb => $sql ) {
    echo "Insert data to admin ";
    $pdo->query($sql);
    echo "..................... done!\n";
}
