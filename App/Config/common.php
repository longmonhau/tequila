<?php
return [
    'TEMPLATE_ENGINE'=>'Twig',
    'TEMPLATE_THEME' => 'white',
    'TEMPLATE_DIR' => APP_PATH.'/Theme/Template',
    'TEMPLATE_CACHE' => false, //in production environment set it to true
    'TEMPLATE_CACHE_DIR' => APP_PATH.'/Theme/Compiled_Template',
    'TEMPLATE_DEBUG' => true, 
    'TEMPLATE_CHARSET'=>'utf-8',
    'TEMPLATE_AUTOESCAPE' => false, //As of Twig 1.8, you can set the escaping strategy to use (html, js, false to disable).
                                    //As of Twig 1.9, you can set the escaping strategy to use (css, url, html_attr, or a PHP callback that takes the template "filename" and must return the escaping strategy to use -- the callback cannot be a function name to avoid collision with built-in escaping strategies).
    'TEMPLATE_OPTIMIZATION' => -1,
    'TEMPLATE_SUFFIX' => '.html',

    'APP_MCRYPT_SALT' => 'Oh!tequiLa'
];