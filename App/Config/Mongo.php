<?php
/**
 * Mongodb Configure
 */
return array(
    "default" => array(
        'host' => 'localhost',
        'port' => '27017',
        'dbname' => 'tequila'
    )     
);