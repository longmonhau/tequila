<?php
return array(
    'index' => ['GET','/', ["Index","index"]],
    
    'signin' => ['GET','/user/sign.in', ['User','sign']],
    
    'viewpost' => ['GET','/blog/{slug}',['Post','view']],
    
    ['POST','/user/login',['User','login']],
    
    ['GET','/user/captcha',['User','captcha']],

    'adminpage'=>["GET","/admin",["Admin\Dashboard","index",["Auth"]]],
    
    'adminpost'=>["GET",'/admin/post',["Admin\PostManage","index",["Auth"]]]
);