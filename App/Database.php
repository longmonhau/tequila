<?php
use Illuminate\Container\Container;  
use Illuminate\Database\Capsule\Manager as Capsule;

$Database = [
	'default'=> [
		'driver'    => 'mysql',
		'host'      => 'localhost',
		'database'  => 'tequila',
		'username'  => 'root',
		'password'  => '',
		'charset'   => 'utf8',
		'collation' => 'utf8_unicode_ci',
		'prefix'    => '',
	]
];

$Capsule = new Capsule;
foreach ($Database as $name => $db) {
	$Capsule->addConnection( $db, $name );
}
$Capsule->setAsGlobal();
$Capsule->bootEloquent();