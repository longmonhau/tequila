# tequila
`The PHP Blog Program`

###Requiments
- [mongo(MongoDB的PHP驱动)](http://php.net/manual/zh/book.mongo.php)
- [SeasLog(PHP日志扩展)](https://github.com/Neeke/SeasLog)

###Hack
###中间件
- 系统中间件都在目录`Kernel/MidWare`中。
- 自定义中间件在目录`Usr/MidWare`中
- 所有中件都继承自抽象类:`lOngmon\Hau\Kernel\MidWare\MidWareAbstract`
- 并实现抽象类未实现的三个抽象函数。也可重写run 函数
- 添加中间件需在`lOngmon\Hau\Kernel\Provider类中midwareRegistry`函数添加


###依赖项目及鸣谢：
- [nikic/fast-route(快速路由)](https://github.com/nikic/FastRoute)
- [Twig(模板引擎)](http://twig.sensiolabs.org)
- [symfony(企业级PHP开发框架)](http://symfony.com)
- [Eloquent(Laravel的ORM)](http://laravel.com/)
- [gregwar/captcha(PHP验证码)](https://github.com/Gregwar/Captcha)
- [Editor.md(markdown编辑器)](https://pandao.github.io/editor.md/)
- [mongo(MongoDB的PHP驱动)](http://php.net/manual/zh/book.mongo.php)
- [SeasLog(PHP日志扩展)](https://github.com/Neeke/SeasLog)