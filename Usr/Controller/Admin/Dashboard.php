<?php namespace lOngmon\Hau\Usr\Controller\Admin;

use lOngmon\Hau\Kernel\Control;

class Dashboard extends Control {
	public function index() {
		$this->render('dashboard.html');
	}
}