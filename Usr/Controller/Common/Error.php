<?php namespace lOngmon\Hau\Usr\Controller\Common;

use lOngmon\Hau\Kernel\Control;

class Error extends Control {
    public function _40x($code) {
        $this->render("error.html");
    }
}