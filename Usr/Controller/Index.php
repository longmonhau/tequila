<?php namespace lOngmon\Hau\Usr\Controller;

use lOngmon\Hau\Kernel\Control;
use lOngmon\Hau\Kernel\Component\AES;
use lOngmon\Hau\Kernel\Component\Config;
use lOngmon\Hau\Kernel\Service\tRedis;

class Index extends Control {
	public function index(){
		return $this->render('index.html');
	}
}