<?php namespace lOngmon\Hau\Usr\Controller;

use lOngmon\Hau\Kernel\Control;
use lOngmon\Hau\Kernel\Component\Captcha;
use lOngmon\Hau\Usr\Model\AdminModel;
use lOngmon\Hau\Usr\Bundle\Session;

class User extends Control {
    public function index() {}
    public function sign() {
        $this->assign('captcha_src',Captcha::newInstance()->build(100,40)->Base64enCode());
        $this->render( 'sign.html' );
    }
    
    public function login( \lOngmon\Hau\Kernel\Http\Request $request ) {
        $name = $request->request->get('name');
        $passwd = $request->request->get('passwd');
        $captcha = $request->request->get('captcha');
        $rememberMe = $request->request->get('remem');
        if ( $_SESSION['Captcha_Code'] !== $captcha ) {
              $resp = ['code'=>402, 'errmsg'=>'验证码错误'];
              return $this->renderJson( $resp );
        }
        $adminModel = new AdminModel();
        if ( $user = $adminModel->getUserByName( $name ) ) {
              if ( \password_verify( $passwd, $user->password ) ) {
                      $usr = [
                            'name'=>$user->name,
                            'email'=>$user->email,
                            'rule' => $user->rule,
                            'last_login_at'=> $user->last_login_at,
                            'last_login_ip' => $user->last_login_ip
                     ];
                      $session = new Session;
                      $session->login( $usr, $_SERVER, $rememberMe );
                      $resp = ['code'=>200, 'go_url'=>'/admin','errmsg'=>'ok'];
                      return $this->renderJson( $resp );
              } else {
                      $resp = ['code'=>405, 'errmsg'=>'用户不存在或密码错误。'];
                      return $this->renderJson( $resp );
              }
        } else {
            $resp = ['code'=>405,'errmsg'=>'用户不存在或密码不匹配!'];
            return $this->renderJson( $resp );
        }
    }
    
    public function captcha() {
        $this->renderString(Captcha::newInstance()->build(100,40)->Base64enCode());
    }
}