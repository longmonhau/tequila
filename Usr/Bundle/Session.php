<?php namespace lOngmon\Hau\Usr\Bundle;

use lOngmon\Hau\Kernel\Component\tString;
use lOngmon\Hau\Kernel\Component\AES;
use lOngmon\Hau\Kernel\Component\Config;
use lOngmon\Hau\Kernel\Factory;

/**
 * Session manage bundle
 * @package Usr/Bundle
 * @author lOngmon Hau <longmon.hau@gmail.com>
 */
class Session{
	
	private $Sess = null;
	
	public function __construct(){
		$this->Sess = Factory::make('Session');
	}
	
	/**
	 * 登陆设置Session 和 Cookie
	 * @param $user array 用户信息
	 * @param $server array $_SERVER 变量
	 * @param $remember bool 是否记住登陆状态
	 * @return void
	 */
	public function login( array $user, array $server, $remember = false ) {
		setCookie( $this->Sess->get('_login_cookie_key'),'', time()-1 );
		$user_agent = str_replace(' ','',$server['HTTP_USER_AGENT'] );
		$this->Sess->set('_login_user_agent', $user_agent );
		$this->Sess->set('_login_user', json_encode( $user ) );
		$this->Sess->set('_login_cookie_key', sha1(tString::rand( 8, tString::ALPHA )  ) );
		$_login_cookie_val = \password_hash( json_encode( $user ).$user_agent ,\PASSWORD_DEFAULT );
		if ( $remember == 1 ) {
			$Cookie_expire_time = time() + 3600*720;
			$this->Sess->set('Cookie_expire_time', $Cookie_expire_time );
			setCookie( $this->Sess->get('_login_cookie_key'), $_login_cookie_val, $Cookie_expire_time, '/' );
		} else {
			setCookie( $this->Sess->get('_login_cookie_key'), $_login_cookie_val,0,'/' );
		}
		return ;
	}
	
	/**
	 * 验证登陆
	 * @param void
	 * @return bool
	 */
	public function checkLogin() {
		if ( $this->Sess->has( '_login_user') && 
		     $this->Sess->has( '_login_cookie_key' ) && 
		     $this->Sess->has('_login_user_agent') &&
		     isset( $_COOKIE[$this->Sess->get('_login_cookie_key')] ) ) 
		{
			if (  \password_verify($this->Sess->get('_login_user').$this->Sess->get('_login_user_agent') ,$_COOKIE[$this->Sess->get('_login_cookie_key')] )) {
				//$this->updateLogin();
				return true;
			}
		}
		return false;
	}
	
	public function updateLogin() {
		setCookie( $this->Sess->get('_login_cookie_key'), '', time()-1, '/' );
		$this->Sess->set('_login_cookie_key', sha1(tString::rand( 8, tString::ALPHA ) ) );
		$_login_cookie_val = \password_hash( $this->Sess->get('_login_user').$this->Sess->get('_login_user_agent'), \PASSWORD_DEFAULT);
		if ( $Cookie_expire_time = $this->Sess->get( 'Cookie_expire_time' ) ) {
			setCookie( $this->Sess->get('_login_cookie_key'), $_login_cookie_val, $Cookie_expire_time, '/' );
		} else {
			setCookie( $this->Sess->get('_login_cookie_key'), $_login_cookie_val,0,'/' );
		}
	}

	public function get( $key, $default = null ) {
		return $this->Sess->get( $key, $default );
	}

	public function set( $key, $val ) {
		$this->Sess->set($key,$val);
	}
	
	public function logout(){
		setCookie( $this->Sess->get('_login_cookie_key'), '', time()-1, '/' );
		$this->Sess->clear();
	}
}