<?php namespace lOngmon\Hau\Usr\Model;

use lOngmon\Hau\Service\MongoDB;

class AdminModel{

	protected $table = 'admin';

	private $ReadMongoDB = null;

	public function __construct() {
		$this->ReadMongoDB = MongoDB::DBInstance('read');
	}

	public function getUserByName( $name ) {
		$query = ["name"=>$name];
		$table = $this->table;
		return $this->ReadMongoDB->$table->findOne($query);
	}
}