<?php namespace lOngmon\Hau\Usr\MidWare;

use lOngmon\Hau\Kernel\MidWare\MidWareAbstract;
use lOngmon\Hau\Usr\Bundle\Session;
use lOngmon\Hau\Kernel\Route;

/**
 * User authorize middleware
 * @package  Usr/Midware
 * @author  lOngmon Hau <longmon.hau@gmail.com>
 */
class AdminAuthorize extends MidWareAbstract {
	private $Sess = null;
	protected  function assert() {
		$this->Sess = new Session;
		$isLogined = $this->Sess->checkLogin();
		return $isLogined;
	}
	protected function goNext(){
		$this->Sess->updateLogin();
	}

	protected function falseHandler(){
		Route::redirect('signin');
		exit;
	}
}